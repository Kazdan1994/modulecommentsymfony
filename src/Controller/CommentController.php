<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\Type\CommentType;
use App\Repository\CommentRepository;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/", name="comment")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function new(Request $request)
    {
        $comments = $this->getDoctrine()
            ->getRepository(Comment::class)->findAll();

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $comment->setCreatedAt(new \DateTime('now'));
            $comment->setUser($user);
            $commentManager = $this->getDoctrine()->getManager();
            $commentManager->persist($comment);
            $commentManager->flush();

            return $this->redirectToRoute('comment');
        }

        return $this->render('comment/index.html.twig', [
            'comments'  =>  $comments,
            'form'      =>  $form->createView()
        ]);
    }

    /**
     * @Route("/comment/{id}", name="show")
     * @param int $id
     * @return Response
     */
    public function show(int $id) {
        $comment = $this->getDoctrine()
            ->getRepository(Comment::class)->find($id);

        $form = $this->createForm(CommentType::class, new Comment());

        return $this->render('comment/show.html.twig', [
            'comment'   =>  $comment,
            'form'      =>  $form->createView()
        ]);
    }
}
