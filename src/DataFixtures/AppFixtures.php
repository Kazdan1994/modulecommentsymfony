<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    /**
     * @var Factory
     */
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 3; $i++) {
            $user = new User();

            $user->setName("John Doe$i");
            $user->setEmail("john$i@doe.fr");
            $user->setPassword(password_hash('password', PASSWORD_BCRYPT));
            $user->setCreatedAt(new \DateTime('now'));
            $user->setRoles(['ROLE_USER']);
            $manager->persist($user);

            for ($j = 0; $j < 5; $j++) {
                $comment = new Comment();

                $comment->setBody($this->faker->text(100));
                $comment->setCreatedAt(new \DateTime('now'));
                $comment->setUser($user);
                $manager->persist($comment);
            }
        }
        $manager->flush();
    }
}
